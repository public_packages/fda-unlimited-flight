package main

import (
	"fmt"
	"time"
)

type flight struct {
	Flight    string
	Departure string
	Arrival   string
	Takeoff   time.Time
	Landing   time.Time
}

func (f *flight) String() string {
	return fmt.Sprintf("%s %s %s %s %s", f.Flight, f.Departure, f.Arrival, f.Takeoff.Format("15:04"), f.Landing.Format("15:04"))
}

func timeParse(v string) time.Time {
	t, _ := time.Parse("15:04", v)
	return t
}

/*
// 2020.10.25 ～ 2021.03.27 のフライト プラン
var flights []*flight = []*flight{
	{"301", "名古屋", "福岡", timeParse("07:05"), timeParse("08:40")},
	{"351", "名古屋", "花巻", timeParse("07:15"), timeParse("08:25")},
	{"341", "名古屋", "高知", timeParse("07:25"), timeParse("08:30")},
	{"361", "名古屋", "青森", timeParse("07:40"), timeParse("09:00")},
	{"411", "名古屋", "出雲", timeParse("07:50"), timeParse("08:55")},
	{"321", "名古屋", "熊本", timeParse("08:05"), timeParse("09:35")},
	{"303", "名古屋", "福岡", timeParse("08:40"), timeParse("10:15")},
	{"381", "名古屋", "山形", timeParse("09:10"), timeParse("10:15")},
	{"363", "名古屋", "青森", timeParse("10:30"), timeParse("11:50")},
	{"353", "名古屋", "花巻", timeParse("10:50"), timeParse("12:00")},
	{"373", "名古屋", "新潟", timeParse("11:00"), timeParse("11:55")},
	{"383", "名古屋", "山形", timeParse("11:40"), timeParse("12:45")},
	{"305", "名古屋", "福岡", timeParse("11:50"), timeParse("13:25")},
	{"345", "名古屋", "高知", timeParse("12:05"), timeParse("13:10")},
	{"325", "名古屋", "熊本", timeParse("12:40"), timeParse("14:10")},
	{"355", "名古屋", "花巻", timeParse("14:35"), timeParse("15:45")},
	{"417", "名古屋", "出雲", timeParse("16:35"), timeParse("17:40")},
	{"367", "名古屋", "青森", timeParse("16:45"), timeParse("18:05")},
	{"357", "名古屋", "花巻", timeParse("16:55"), timeParse("18:05")},
	{"313", "名古屋", "福岡", timeParse("17:45"), timeParse("19:20")},
	{"327", "名古屋", "熊本", timeParse("17:55"), timeParse("19:25")},
	{"347", "名古屋", "高知", timeParse("18:20"), timeParse("19:25")},
	{"315", "名古屋", "福岡", timeParse("19:55"), timeParse("21:30")},
	{"212", "千歳", "松本", timeParse("14:00"), timeParse("15:50")},
	{"603", "千歳", "山形", timeParse("15:10"), timeParse("16:30")},
	{"362", "青森", "名古屋", timeParse("09:35"), timeParse("11:05")},
	{"834", "青森", "神戸", timeParse("12:25"), timeParse("14:15")},
	{"364", "青森", "名古屋", timeParse("14:00"), timeParse("15:30")},
	{"368", "青森", "名古屋", timeParse("18:45"), timeParse("20:15")},
	{"352", "花巻", "名古屋", timeParse("09:00"), timeParse("10:25")},
	{"354", "花巻", "名古屋", timeParse("14:00"), timeParse("14:00")},
	{"356", "花巻", "名古屋", timeParse("16:20"), timeParse("17:45")},
	{"358", "花巻", "名古屋", timeParse("18:40"), timeParse("20:05")},
	{"382", "山形", "名古屋", timeParse("10:55"), timeParse("12:10")},
	{"602", "山形", "千歳", timeParse("13:20"), timeParse("14:35")},
	{"386", "山形", "名古屋", timeParse("17:05"), timeParse("18:20")},
	{"703", "仙台", "出雲", timeParse("12:15"), timeParse("14:10")},
	{"504", "新潟", "福岡", timeParse("12:55"), timeParse("15:00")},
	{"374", "新潟", "名古屋", timeParse("17:25"), timeParse("18:25")},
	{"233", "松本", "神戸", timeParse("09:50"), timeParse("10:55")},
	{"211", "松本", "千歳", timeParse("11:35"), timeParse("13:10")},
	{"205", "松本", "福岡", timeParse("12:45"), timeParse("14:35")},
	{"207", "松本", "福岡", timeParse("16:25"), timeParse("18:15")},
	{"141", "静岡", "福岡", timeParse("07:35"), timeParse("09:30")},
	{"181", "静岡", "出雲", timeParse("08:25"), timeParse("09:50")},
	{"143", "静岡", "福岡", timeParse("09:00"), timeParse("10:55")},
	{"193", "静岡", "北九州", timeParse("11:20"), timeParse("13:00")},
	{"133", "静岡", "鹿児島", timeParse("13:25"), timeParse("15:10")},
	{"147", "静岡", "福岡", timeParse("15:35"), timeParse("17:30")},
	{"149", "静岡", "福岡", timeParse("17:40"), timeParse("19:35")},
	{"821", "神戸", "高知", timeParse("07:25"), timeParse("08:10")},
	{"232", "神戸", "松本", timeParse("10:00"), timeParse("11:00")},
	{"833", "神戸", "青森", timeParse("11:45"), timeParse("13:20")},
	{"817", "神戸", "出雲", timeParse("16:00"), timeParse("16:55")},
	{"827", "神戸", "高知", timeParse("18:00"), timeParse("18:45")},
	{"412", "出雲", "名古屋", timeParse("09:25"), timeParse("10:20")},
	{"702", "出雲", "仙台", timeParse("10:20"), timeParse("11:45")},
	{"816", "出雲", "神戸", timeParse("14:40"), timeParse("15:30")},
	{"188", "出雲", "静岡", timeParse("17:25"), timeParse("18:35")},
	{"418", "出雲", "名古屋", timeParse("18:10"), timeParse("19:05")},
	{"822", "高知", "神戸", timeParse("08:40"), timeParse("09:25")},
	{"342", "高知", "名古屋", timeParse("09:00"), timeParse("10:00")},
	{"346", "高知", "名古屋", timeParse("13:40"), timeParse("14:40")},
	{"828", "高知", "神戸", timeParse("19:15"), timeParse("20:00")},
	{"348", "高知", "名古屋", timeParse("19:55"), timeParse("20:55")},
	{"194", "北九州", "静岡", timeParse("13:45"), timeParse("15:00")},
	{"200", "福岡", "松本", timeParse("07:50"), timeParse("09:15")},
	{"142", "福岡", "静岡", timeParse("09:15"), timeParse("10:40")},
	{"302", "福岡", "名古屋", timeParse("10:00"), timeParse("12:20")},
	{"202", "福岡", "松本", timeParse("10:45"), timeParse("12:10")},
	{"144", "福岡", "静岡", timeParse("11:25"), timeParse("12:50")},
	{"306", "福岡", "名古屋", timeParse("13:55"), timeParse("15:15")},
	{"505", "福岡", "新潟", timeParse("15:10"), timeParse("16:50")},
	{"308", "福岡", "名古屋", timeParse("15:55"), timeParse("17:15")},
	{"148", "福岡", "静岡", timeParse("18:00"), timeParse("19:25")},
	{"312", "福岡", "名古屋", timeParse("18:45"), timeParse("20:05")},
	{"150", "福岡", "静岡", timeParse("19:50"), timeParse("21:15")},
	{"314", "福岡", "名古屋", timeParse("20:10"), timeParse("21:30")},
	{"322", "熊本", "名古屋", timeParse("10:15"), timeParse("11:35")},
	{"326", "熊本", "名古屋", timeParse("14:50"), timeParse("16:10")},
	{"328", "熊本", "名古屋", timeParse("20:05"), timeParse("21:25")},
	{"134", "鹿児島", "静岡", timeParse("15:40"), timeParse("17:05")},
}
*/
// 2021.03.28 ～ 2021.10.30 のフライト プラン
var flights []*flight = []*flight{
	{"133", "静岡", "鹿児島", timeParse("13:15"), timeParse("14:50")},
	{"134", "鹿児島", "静岡", timeParse("15:20"), timeParse("16:50")},
	{"141", "静岡", "福岡", timeParse("07:35"), timeParse("09:20")},
	{"142", "福岡", "静岡", timeParse("09:50"), timeParse("11:15")},
	{"143", "静岡", "福岡", timeParse("09:00"), timeParse("10:45")},
	{"144", "福岡", "静岡", timeParse("10:55"), timeParse("12:20")},
	{"147", "静岡", "福岡", timeParse("16:40"), timeParse("18:25")},
	{"148", "福岡", "静岡", timeParse("18:35"), timeParse("20:00")},
	{"149", "静岡", "福岡", timeParse("17:50"), timeParse("19:35")},
	{"150", "福岡", "静岡", timeParse("20:00"), timeParse("21:25")},
	{"173", "静岡", "丘珠", timeParse("11:45"), timeParse("13:30")},
	{"174", "丘珠", "静岡", timeParse("14:10"), timeParse("16:05")},
	{"181", "静岡", "出雲", timeParse("08:20"), timeParse("09:35")},
	{"188", "出雲", "静岡", timeParse("16:00"), timeParse("17:10")},
	{"200", "福岡", "松本", timeParse("07:55"), timeParse("09:25")},
	{"204", "福岡", "松本", timeParse("12:20"), timeParse("13:50")},
	{"205", "松本", "福岡", timeParse("14:25"), timeParse("16:05")},
	{"207", "松本", "福岡", timeParse("16:25"), timeParse("18:05")},
	{"211", "松本", "千歳", timeParse("11:30"), timeParse("13:10")},
	{"212", "千歳", "松本", timeParse("14:15"), timeParse("15:55")},
	{"232", "神戸", "松本", timeParse("10:00"), timeParse("11:00")},
	{"233", "松本", "神戸", timeParse("09:55"), timeParse("11:00")},
	{"302", "名古屋", "福岡", timeParse("07:00"), timeParse("08:25")},
	{"302", "福岡", "名古屋", timeParse("09:00"), timeParse("10:20")},
	{"303", "名古屋", "福岡", timeParse("08:30"), timeParse("09:55")},
	{"304", "福岡", "名古屋", timeParse("11:15"), timeParse("12:35")},
	{"305", "名古屋", "福岡", timeParse("10:20"), timeParse("11:45")},
	{"310", "福岡", "名古屋", timeParse("16:55"), timeParse("18:15")},
	{"312", "福岡", "名古屋", timeParse("18:55"), timeParse("20:15")},
	{"313", "名古屋", "福岡", timeParse("17:55"), timeParse("19:20")},
	{"314", "福岡", "名古屋", timeParse("20:05"), timeParse("21:25")},
	{"315", "名古屋", "福岡", timeParse("20:05"), timeParse("21:30")},
	{"321", "名古屋", "熊本", timeParse("08:00"), timeParse("09:25")},
	{"322", "熊本", "名古屋", timeParse("10:05"), timeParse("11:25")},
	{"325", "名古屋", "熊本", timeParse("12:25"), timeParse("13:50")},
	{"326", "熊本", "名古屋", timeParse("14:50"), timeParse("16:10")},
	{"327", "名古屋", "熊本", timeParse("18:05"), timeParse("19:30")},
	{"328", "熊本", "名古屋", timeParse("20:10"), timeParse("21:30")},
	{"341", "名古屋", "高知", timeParse("07:20"), timeParse("08:20")},
	{"342", "高知", "名古屋", timeParse("08:50"), timeParse("09:50")},
	{"345", "名古屋", "高知", timeParse("11:55"), timeParse("12:55")},
	{"346", "高知", "名古屋", timeParse("16:00"), timeParse("17:00")},
	{"347", "名古屋", "高知", timeParse("18:25"), timeParse("19:25")},
	{"348", "高知", "名古屋", timeParse("19:55"), timeParse("20:55")},
	{"351", "名古屋", "花巻", timeParse("07:10"), timeParse("08:20")},
	{"352", "花巻", "名古屋", timeParse("08:50"), timeParse("10:05")},
	{"353", "名古屋", "花巻", timeParse("10:35"), timeParse("11:45")},
	{"354", "花巻", "名古屋", timeParse("12:15"), timeParse("13:30")},
	{"355", "名古屋", "花巻", timeParse("13:10"), timeParse("14:20")},
	{"356", "花巻", "名古屋", timeParse("17:30"), timeParse("18:45")},
	{"357", "名古屋", "花巻", timeParse("17:00"), timeParse("18:10")},
	{"358", "花巻", "名古屋", timeParse("18:40"), timeParse("19:55")},
	{"361", "名古屋", "青森", timeParse("07:35"), timeParse("08:55")},
	{"362", "青森", "名古屋", timeParse("09:25"), timeParse("10:50")},
	{"363", "名古屋", "青森", timeParse("11:25"), timeParse("12:45")},
	{"364", "青森", "名古屋", timeParse("14:05"), timeParse("15:30")},
	{"365", "名古屋", "青森", timeParse("14:10"), timeParse("15:30")},
	{"366", "青森", "名古屋", timeParse("16:00"), timeParse("17:25")},
	{"367", "名古屋", "青森", timeParse("17:30"), timeParse("18:50")},
	{"368", "青森", "名古屋", timeParse("19:20"), timeParse("20:45")},
	{"373", "名古屋", "新潟", timeParse("10:45"), timeParse("11:40")},
	{"374", "新潟", "名古屋", timeParse("16:50"), timeParse("17:50")},
	{"381", "名古屋", "山形", timeParse("09:10"), timeParse("10:15")},
	{"382", "山形", "名古屋", timeParse("10:45"), timeParse("11:55")},
	{"383", "名古屋", "山形", timeParse("11:05"), timeParse("12:10")},
	{"386", "山形", "名古屋", timeParse("16:20"), timeParse("17:30")},
	{"411", "名古屋", "出雲", timeParse("07:45"), timeParse("08:45")},
	{"412", "出雲", "名古屋", timeParse("09:15"), timeParse("10:15")},
	{"417", "名古屋", "出雲", timeParse("16:45"), timeParse("17:45")},
	{"418", "出雲", "名古屋", timeParse("18:15"), timeParse("19:15")},
	{"602", "山形", "千歳", timeParse("12:40"), timeParse("14:00")},
	{"603", "千歳", "山形", timeParse("14:35"), timeParse("15:50")},
	{"702", "出雲", "仙台", timeParse("10:05"), timeParse("11:30")},
	{"703", "仙台", "出雲", timeParse("12:00"), timeParse("13:35")},
	{"824", "高知", "神戸", timeParse("13:25"), timeParse("14:10")},
	{"825", "神戸", "高知", timeParse("14:45"), timeParse("15:30")},
}

var plan []flight
var result [][]flight

func get_continue(f *flight) [][]*flight {
	var result [][]*flight
	var possible_time = f.Landing.Add(20 * time.Minute)

	for _, cont := range flights {
		if cont.Departure == f.Arrival && possible_time.Before(cont.Takeoff) {
			for _, plan := range get_continue(cont) {
				result = append(result, append([]*flight{f}, plan...))
			}
		}
	}
	if result == nil {
		result = append(result, []*flight{f})
	}
	return result
}

func contains(target []string, item string) bool {
	for _, t := range target {
		if t == item {
			return true
		}
	}
	return false
}

func main() {
	var airports []string
	for _, f := range flights {
		if !contains(airports, f.Departure) {
			airports = append(airports, f.Departure)
		}
	}

	for _, start := range airports {
		fmt.Printf("BEGIN from %s\n--\n", start)
		for _, f := range flights {
			if f.Departure == start {
				for _, p := range get_continue(f) {
					for _, ff := range p {
						fmt.Printf("%v\n", ff)
					}
					fmt.Printf("--\n")
				}
			}
		}
		fmt.Printf("END from %s\n--\n", start)
	}
	fmt.Println("ALL END")
}
